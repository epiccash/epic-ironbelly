#Change these paths to match the correspondent files and folders on
#your computer. These paths here were based on mac os
export NDK_PREBUILT=$ANDROID_NDK/toolchains/llvm/prebuilt/darwin-x86_64
export NDK_USR=$ANDROID_NDK/platforms/android-23/arch-x86/usr/

#Check if paths declared below appoint to the right files
#(i.e i686-linux-android23-clang, i686-linux-android-as)
export TARGET_CC=$NDK_PREBUILT/bin/i686-linux-android23-clang
export TARGET_CXX=$NDK_PREBUILT/bin/i686-linux-android23-clang++
export TARGET_LD=$NDK_PREBUILT/bin/i686-linux-android-ld
export TARGET_AR=$NDK_PREBUILT/bin/i686-linux-android-ar
export TARGET_AS=$NDK_PREBUILT/bin/i686-linux-android-as
export TARGET_CFLAGS="-DMDB_USE_ROBUST=0  -fPIC -fpic -fno-integrated-as"
export TARGET_CXXFLAGS="-DMDB_USE_ROBUST=0 -I$NDK_USR -fpic -fPIC -fno-integrated-as"
export TARGET_CPPFLAGS="-DMDB_USE_ROBUST=0 -I$NDK_USR  -fpic -fPIC -fno-integrated-as"
export TARGET_LDFLAGS="-Wl,-rpath-link=$NDK_USR/lib/ -L$NDK_USR/lib/"
export LIBS="-lc".

#Change these paths to match the correspondent files and folders on
#your computer. These paths here were based on mac os
export PATH="$HOME/.NDK/x86/bin:$PATH"
cargo build --target=i686-linux-android --release --no-default-features
cp target/i686-linux-android/release/libwallet.so ../android/app/src/main/jniLibs/x86/
echo "copied the file to jniLibs"
#After running this script, copy the target/i686-linux-android/release/libwallet.so to your jniLibs folder on android