
#Change these paths to match the correspondent files and folders on
#your computer. These paths here were based on mac os
export NDK_PREBUILT=$ANDROID_NDK/toolchains/llvm/prebuilt/darwin-x86_64
export NDK_USR=$ANDROID_NDK/platforms/android-23/arch-arm/usr/

#Check if paths declared below appoint to the right files
#(i.e armv7a-linux-android23-clang, arm-linux-android-ld)
export TARGET_CC=$NDK_PREBUILT/bin/armv7a-linux-androideabi23-clang
export TARGET_CXX=$NDK_PREBUILT/bin/armv7a-linux-androideabi23-clang++
export TARGET_LD=$NDK_PREBUILT/bin/arm-linux-androideabi-ld
export TARGET_AR=$NDK_PREBUILT/bin/arm-linux-androideabi-ar
export TARGET_AS=$NDK_PREBUILT/bin/arm-linux-androideabi-as
export TARGET_CFLAGS="-DMDB_USE_ROBUST=0 -nostdlib -fPIC -fpic"
export TARGET_CXXFLAGS="-DMDB_USE_ROBUST=0 -I$NDK_USR -nostdlib -fpic -fPIC"
export TARGET_CPPFLAGS="-DMDB_USE_ROBUST=0 -I$NDK_USR -nostdlib -fpic -fPIC"
export TARGET_LDFLAGS="-Wl,-rpath-link=$NDK_USR/lib/ -L$NDK_USR/lib/"
export LIBS="-lc".

#Change these paths to match the correspondent files and folders on
#your computer. These paths here were based on mac os
export PATH="$HOME/.NDK/arm/bin:$PATH"
cargo build --target=armv7-linux-androideabi --release --no-default-features
#cp ../target/armv7-linux-androideabi/release/libepicwallet.so /Users/leo/Tandroid/MyApplication/app/src/main/jniLibs/armeabi-v7a/libepicwallet.so
#echo "Copied the file!"