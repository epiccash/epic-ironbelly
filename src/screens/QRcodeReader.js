
import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { processColor, StatusBar,Modal,TouchableHighlight } from 'react-native'
import { Text, View, Alert } from 'react-native';
import FormTextInput from 'components/FormTextInput'
import NumericInput from 'components/NumericInput'
import { HeaderBackButton } from 'react-navigation'
import colors from 'common/colors'
import { isAndroid, Spacer } from 'common'
import styled from 'styled-components/native'
import { Button } from 'components/CustomFont'
import SwitchSelector from "react-native-switch-selector";
import QRCode from 'react-native-qrcode';







type Props = {
  navigation: Navigation,
  setFromLink: (amount: number, message: string, url: string) => void,
  setHttpAddress: (url: string) => void,
}
const Submit = styled(Button)``


class QRcodeReader extends Component<Props,State>{
	static navigationOptions = {
    title: 'QRReader',
    headerLeft: ({ scene }) => {
      return (
        <HeaderBackButton
          tintColor={colors.black}
          backTitleVisible={!isAndroid}
          onPress={() => scene.descriptor.navigation.goBack(null)}
        />
      )
    },
  }
  state = { 
  	isEpicCash: true,
  	setAmount: 0,
	  modalVisible: false,
    OptionalMessage: "",
   };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  } 



  renderElement = (props) => {
   if(!this.state.isEpicCash)
      return (<Text>This feature is a work on progress </Text>);
   else{
   		 return(
   		 		<FormTextInput
              testID="Amount"
              returnKeyType={'next'}
              autoFocus={true}
              secureTextEntry={false}
              onChange={(EpicValue) => this.setState({setAmount: EpicValue}) }
              value={this.state.setAmount}
              title="Amount in EpicCash*"
              placeholder="438.25"
              keyboardType="number"
            />
   		 	);
   }
   // if(this.state.isEpicCash){
   // 	return(<FormTextInput
   //            testID="Amount"
   //            returnKeyType={'next'}
   //            autoFocus={true}
   //            secureTextEntry={false}
   //            onChange={setAmount}
   //            value={Dolarvalue}
   //            title="Amount in Dollar*"
   //            placeholder="438.25"
   //          />);

   // }
     
	}



	render(){
		return (
	       <View style={{ flex: 1 }}>
	       	<View style={{ flexGrow: 1, flexShrink: 1, flexBasis: '10%' }} />
	       	<SwitchSelector
			  initial={0}
			  onPress={value => this.setState({ isEpicCash: value })}
			  textColor={colors.black} //'#7a44cf'
			  selectedColor={colors.white}
			  buttonColor={colors.purple}
			  borderColor={colors.purple}
			  hasPadding
			  options={[
			    { label: "EpicCash", value: true}, //images.feminino = require('./path_to/assets/img/feminino.png')
			    { label: "Dollar", value: false} //images.masculino = require('./path_to/assets/img/masculino.png')
			  ]}
			/>
            <View style={{ flexGrow: 1, flexShrink: 1, flexBasis: '10%' }} />
            <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
          	this.setModalVisible(!this.state.modalVisible);
            Alert.alert('Modal has been closed.');
          }}>
          <View style={{ flex: 1 }}>
	       	<View style={{ flexGrow: 1, flexShrink: 1, flexBasis: '10%',alignItems: 'center', justifyContent: 'center'}}>
              	 <QRCode
			          value={this.state.setAmount + this.state.OptionalMessage}
			          size={200}
			          bgColor='purple'
			          fgColor='white'/>
			 </View>

			<View style={{ flexGrow: 1, flexShrink: 1, flexBasis: '10%' }} />

              <Submit
            title={'Close QRCode'}
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}/>
            </View>
        </Modal>



            { this.renderElement() }
         	      

            

	        
            <FormTextInput
              testID="Amount"
              returnKeyType={'next'}
              autoFocus={true}
              secureTextEntry={false}
              onChange={(UserMessage) => this.setState({OptionalMessage: UserMessage})}
              value={this.state.OptionalMessage}
              title="MessageBox"
              placeholder="Write your message here(optional)"
              multiline = {true}
            />
            <View style={{ flexGrow: 1, flexShrink: 1, flexBasis: '20%' }} />
            
            <Submit
            title={'Generate QR code'}
            //disabled={inProgress}
              onPress={() => {
              		if(this.state.setAmount > 0){
              	 		this.setModalVisible(true);
              		}
              		else{
              			Alert.alert(
						  'QR Generator ',
						  'Please fill the amount you want to send',
						  [
						    {
						      text: 'Cancel',
						      onPress: () => console.log('Cancel Pressed'),
						      style: 'cancel',
						    },
						  ],
						  {cancelable: false},
						);

              		}
                
              }}
            />
	      	<View style={{ flexGrow: 10, flexShrink: 0, flexBasis: 16 }} />
          </View>
    	);
	}
}


export default QRcodeReader